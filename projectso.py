24 de 4.354
Projeto - SO
Caixa de entrada
	x
Gabriel Terry de Souza Cavalcante
	
Anexos9 de nov. de 2019 08:43 (há 2 dias)
	
para edson.creeper, eu
   
Traduzir mensagem
Desativar para: inglês
Trabalho concluído.
Área de anexos
	
	
	

import threading
import random

class Conta():  # Classe das contas
    def __init__(self, saldo):
        self.saldo = saldo

class MinhaThread(threading.Thread, Conta):
    def __init__(self, meuId, saldo_from, saldo_to, trava, valor, op):
        self.meuId = meuId  # Identificador das Threads (Tranferências)
        self.saldo_conta_from = saldo_from  # Saldo da conta de saída
        self.saldo_conta_to = saldo_to  # Saldo da conta de entrada
        self.trava = trava  # Variável de trava
        self.valor = valor  # Valor das tranferências
        self.operacao = op  # Decisão de qual conta o dinheiro sai
        threading.Thread.__init__(self)

    def run(self):    # Parte responsável pelo que a thread vai executar
        with self.trava:   # Parte responsável por travar e liberar as threads (acquire, release)
            print(f'Saldo da conta 1 antes da tranferência: R${conta1.saldo}'
                  f'\nSaldo da conta 2 antes da transferência: R${conta2.saldo}\n')

            if self.saldo_conta_from >= self.valor and self.operacao == 1:  # Tranferência da conta 1 para a 2
                conta1.saldo -= self.valor
                conta2.saldo += self.valor
                print(f'Tranferência {self.meuId} de R${self.valor} da conta 1 para a conta 2 concluída com sucesso!'
                      f'\nSaldo da conta 1 após tranferência: R${conta1.saldo}'
                      f'\nSaldo da conta 2 após tranferência: R${conta2.saldo}\n')
            elif self.saldo_conta_from < self.valor and self.operacao == 1:  # Tratamento de excessão
                print(f'Não foi possível fazer a tranferência {self.meuId} de R${self.valor} da conta 1 para a conta 2.\n')

            elif self.saldo_conta_from >= self.valor and self.operacao == 2:  # Tranferência da conta 2 para a 1
                conta2.saldo -= self.valor
                conta1.saldo += self.valor
                print(f'Tranferência {self.meuId} de R${self.valor} da conta 2 para a conta 1 concluída com sucesso!'
                      f'\nSaldo da conta 1 após tranferência: R${conta1.saldo}'
                      f'\nSaldo da conta 2 após tranferência: R${conta2.saldo}\n')
            elif self.saldo_conta_from < self.valor and self.operacao == 2:  # Tratamento de excessão
                print(f'Não foi possível fazer a tranferência {self.meuId} de R${self.valor} da conta 2 para a conta 1.\n')

if __name__ == '__main__':
    trava = threading.Lock()  # Trava resposável pelo controle das Threads
    threads = []  # Lista para guardar as threads
    conta1 = Conta(100)  # Definição do valor inicial da conta 1
    conta2 = Conta(100)  # Definição do valor inicial da conta 2
    id = 0
    val = random.randint(1, 100)  # Sorteio de quanto dinheiro será tranferido
    op = random.randint(1, 2)  # Sorteio de decisão de qual conta o dinheiro vai sair
    for id in range(100):  # Definição de quantas tranferências serão executadas
        val = random.randint(1, 100)
        op = random.randint(1, 2)

        if op == 1:
            thread = MinhaThread(id, conta1.saldo, conta2.saldo, trava, val, op)  # Isso cria a thread
            thread.start()  # Isso executa ela
            threads.append(thread)  # Isso joga a thread dentro de uma lista

            for thread in threads:
                thread.join()  #Isso espera cada thread da lista finalizar


        elif op == 2:
            thread = MinhaThread(id, conta2.saldo, conta1.saldo, trava, val, op)  # Isso cria a thread
            thread.start()  # Isso executa ela
            threads.append(thread)  # Isso joga a thread dentro de uma lista

            for thread in threads:
                thread.join()  #Isso espera cada thread da lista finalizar

